﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

using System.IO;
using System.Threading.Tasks;

//==============================================
//      https://docs.microsoft.com/zh-tw/azure/visual-studio/vs-storage-aspnet-getting-started-blobs
//==============================================



namespace Easypay_BlobToDisk
{
    public class BlobFunc
    {
        //==============================================
        //         define
        //==============================================
        private string STORAGE_CONNECTION_STRING = "";//"DefaultEndpointsProtocol=https;AccountName=storageaefile;AccountKey=q4AeiiTZSIOdDJ0BlssKSneQyHyud3NkPMe5QGltlAIog6NfsDHPC9n4byONZ9WMONHehRByhUeBNKcA4Py26Q==;EndpointSuffix=core.windows.net";
        private string CONTAINER_NAME = "";//"test-blob-container";

        //==============================================
        //         register
        //==============================================
        private CloudBlobContainer container = null;
        private String PATH = "";

        //==============================================
        //         initial
        //==============================================
        public BlobFunc(string connectionSting, string container_name, string path)
        {
            this.STORAGE_CONNECTION_STRING = connectionSting;
            this.CONTAINER_NAME = container_name;
            this.PATH = path;

            this.container = GetCloudBlobContainer();
        }



        //==============================================
        //          CreateBlobContainer
        //==============================================
        public void CreateBlobContainer()
        {
            //Create a new container, if it does not exist
            container.CreateIfNotExists();
        }

        //==============================================
        //          GetCloudBlobContainer
        //==============================================
        public CloudBlobContainer GetCloudBlobContainer()
        {
            CloudBlobContainer tcontainer = null;
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(STORAGE_CONNECTION_STRING);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            tcontainer = blobClient.GetContainerReference(CONTAINER_NAME);
            return tcontainer;
        }

        public string UploadBlobAndReturnUrl(string blobFileName)
        {
            CloudBlockBlob blob = container.GetBlockBlobReference(blobFileName);
            using (var fileStream = System.IO.File.OpenRead(PATH + "\\" + blobFileName))
            {
                blob.UploadFromStream(fileStream);
                return blob.Uri.AbsoluteUri;
            }
        }
        //==============================================
        //          GetListBlobs
        //==============================================
        public List<string> GetListBlobs()
        {
            List<string> blobs = new List<string>();
            try
            {
                foreach (IListBlobItem item in container.ListBlobs(useFlatBlobListing: true))
                {
                    if (item.GetType() == typeof(CloudBlockBlob))
                    {
                        CloudBlockBlob nblob = (CloudBlockBlob)item;
                        blobs.Add(nblob.Name);
                    }
                    else if (item.GetType() == typeof(CloudPageBlob))
                    {
                        CloudPageBlob nblob = (CloudPageBlob)item;
                        blobs.Add(nblob.Name);
                    }
                    else if (item.GetType() == typeof(CloudBlobDirectory))
                    {
                        CloudBlobDirectory dir = (CloudBlobDirectory)item;
                        blobs.Add(dir.Uri.ToString());
                    }
                }
            }
            catch (StorageException ex)
            {
                Console.WriteLine("Error returned from the service: {0}", ex.Message);
            }
            finally
            {
            }
            return blobs;
        }

        //==============================================
        //          DownloadBlobs
        //==============================================
        public void DownloadBlobs(string blobFileName, string bkPath)
        {
            try
            {
                //----   DownloadBlob()
                CloudBlockBlob blob_download = container.GetBlockBlobReference(blobFileName);      //   "myBlob.jpg");
                using (var fileStream = System.IO.File.OpenWrite(PATH + "\\" + blobFileName))  //  @"F:\asp_download\myBlob.jpg"))
                {
                    blob_download.DownloadToStream(fileStream);
                }
                
                //backup
                using (var fileStream = System.IO.File.OpenWrite(bkPath + "\\" + blobFileName))  //  @"F:\asp_download\myBlob.jpg"))
                {
                    blob_download.DownloadToStream(fileStream);
                }
            }
            catch (StorageException ex)
            {
                Console.WriteLine("Error returned from the service: {0}", ex.Message);
            }
            finally
            {
                //----   cancel container
                if (container != null)
                {
                    //await container.DeleteIfExistsAsync();
                    //container.DeleteIfExists();
                }
            }
        }



        //==============================================
        //          DeleteBlob
        //==============================================
        public void DeleteBlob(string blobFileName)
        {
            CloudBlockBlob blob_delete = container.GetBlockBlobReference(blobFileName);
            blob_delete.Delete();
        }

        //==============================================
        //          UploadBlob
        //==============================================
        public void UploadBlob(string blobFileName)
        {
            CloudBlockBlob blob = container.GetBlockBlobReference(blobFileName);
            using (var fileStream = System.IO.File.OpenRead(PATH + "\\" + blobFileName))
            {
                blob.UploadFromStream(fileStream);
            }
        }


        //==============================================
        //          FindBlob
        //==============================================
        public int FindBlob(string blobFileName)
        {
            List<string> blobs = GetListBlobs();
            int yes = 0;

            foreach (string blob in blobs)
            {
                if (blob == blobFileName)
                {
                    yes = 1;
                }
            }
            return yes;
        }
    }
}
