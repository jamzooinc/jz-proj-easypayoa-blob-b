﻿using Ionic.Zip;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Easypay_BlobToDisk
{
    class BlobMain
    {
        //=========================================================
        //          Create container
        //=========================================================
        public void NotExistsCreate(string container_name)
        {
            BlobFunc blobFunc = new BlobFunc(Constants.STORAGE_CONNECTION_STRING, container_name, "");
            blobFunc.CreateBlobContainer();

        }

        //=========================================================
        //          Upload
        //=========================================================
        public class ResultStuts
        {
            public String CODE { get; set; }
            public String MSG { get; set; }
        }

        //=========================================================
        //          Upload
        //=========================================================
        public void Upload()
        {
            //Console.WriteLine("Azure Blob storage - .NET Quickstart sample");
            //Console.WriteLine();
            //ProcessAsync().GetAwaiter().GetResult();

            //------------------------------------------------------
            BlobFunc blobFunc = new BlobFunc(Constants.STORAGE_CONNECTION_STRING, Constants.CONTAINER_NAME_OATESTFILE, Constants.DISK_FOLDER_NAME_MAIN);
            List<string> blobs = blobFunc.GetListBlobs();


            //------------------------------------------------------
            FolderFunc folderFunc = new FolderFunc(Constants.DISK_FOLDER_NAME_MAIN);
            List<string> files = folderFunc.GetFolderEntries();

            //------------------------------------------------------
            blobFunc.UploadBlob("myBlob.jpg");
        }

        //=========================================================
        //          Download
        //=========================================================
        public void Download(string container_name, string path, string bkPath)
        {
            //Console.WriteLine("Azure Blob storage - .NET Quickstart sample");
            //Console.WriteLine();
            //ProcessAsync().GetAwaiter().GetResult();


            //------------------------------------------------------
            BlobFunc blobFunc = new BlobFunc(Constants.STORAGE_CONNECTION_STRING, container_name, path);
            List<string> blobs = blobFunc.GetListBlobs();


            //------------------------------------------------------
            FolderFunc folderFunc = new FolderFunc(path);
            List<string> files = folderFunc.GetFolderEntries();

            Console.WriteLine("blobs.Count=" + blobs.Count);
            //------------------------------------------------------
            foreach (string blob in blobs)
            {
                Console.WriteLine("blob=" + blob);
                int yes = 0;
                foreach (string file in files)
                {
                    if (file == blob)
                    {
                        #region new backup name

                        if (File.Exists(file))
                        {
                            string destPath = file.Replace("newcase", "bk-newcase");
                            destPath = file.Replace("othercase", "bk-othercase");
                            destPath = file.Replace("newcase", "bk-newcase");
                            File.Copy(file, destPath);
                        }

                        #endregion

                        yes = 1;
                    }
                }
                if (yes == 0)
                {
                    Console.WriteLine("bkPath : " + bkPath + "\n");
                    blobFunc.DownloadBlobs(blob, bkPath);
                    Console.WriteLine("Download : " + blob + "\n");
                    bool isProduction = ! container_name.ToLower().Contains("test");
                    Console.WriteLine("isProduction : " + isProduction + "\n");
                    var zipFile = bkPath + "\\" + blob;
                    Console.WriteLine("zipFile : " + zipFile + "\n");
                    var zipFolder = bkPath + "\\" + blob.ToLower().Replace(".zip","");
                    Console.WriteLine("zipFolder : " + zipFolder + "\n");
                    //下載後解壓縮將圖片都上傳
                    UploadImageToBlob(zipFile, zipFolder, isProduction);
                    //刪除解壓縮的資料夾 留zip檔案就好
                    Directory.Delete(zipFolder,true);
                    Console.WriteLine("Delete zipFolder ");
                    DeleteBlob(blob, container_name, "DeleteBlobFunctionNoUse");
                    Console.WriteLine("Delete blob ");
                }
            }

             
            //------------------------------------------------------
            //Console.WriteLine("Press any key to exit the sample application.");
            //Console.ReadLine();
        }

        public const string CONTAINER_NAME_IMG = "case-img";
        public const string CONTAINER_NAME_IMG_TEST = "test-case-img";

        private void UploadImageToBlob(string zipPath, string zipDir, bool is_production)
        {
            Console.WriteLine("UploadImageToBlob Start");
            try
            {

                using (var zip = ZipFile.Read(zipPath))//zip file
                {
                    foreach (var zipEntry in zip)
                    {
                        zipEntry.Extract(zipDir, ExtractExistingFileAction.OverwriteSilently);
                    }
                }

                Console.WriteLine("Zip  Extract  Down");
                foreach (string filePath in System.IO.Directory.GetFiles(zipDir))
                {
                    var fnArray = filePath.Split('\\');
                    var fn = fnArray[fnArray.Length - 1].ToUpper();
                    if (fn.Contains(".JPG") ||
                        fn.Contains(".JPEG") ||
                        fn.Contains(".PNG") ||
                        fn.Contains(".BMP") ||
                        fn.Contains(".TIFF") ||
                        fn.Contains(".PCX") ||
                        fn.Contains(".TGA") ||
                        fn.Contains(".EXIF") ||
                        fn.Contains(".FPX") ||
                        fn.Contains(".SVG") ||
                        fn.Contains(".PSD") ||
                        fn.Contains(".CDR") ||
                        fn.Contains(".PCD") ||
                        fn.Contains(".DXF") ||
                        fn.Contains(".UFO") ||
                        fn.Contains(".EPS") ||
                        fn.Contains(".GIF"))
                    {
                        //上傳blob
                        BlobMain blobMain = new BlobMain();
                        var contrainerName = is_production
                            ? CONTAINER_NAME_IMG
                            : CONTAINER_NAME_IMG_TEST;

                        var url = blobMain.UploadAndBlobUrl(contrainerName, zipDir, fnArray[fnArray.Length - 1]);

                        Console.WriteLine("Upload Img Success, Url =" + url);
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("UploadImageToBlob.Exception="+ex.ToString());
            }
        }

        public string UploadAndBlobUrl(string containerName, string zipFolder, string fileName)
        {
            BlobFunc blobFunc = new BlobFunc(
                Constants.STORAGE_CONNECTION_STRING,
                containerName,
                zipFolder);
            return blobFunc.UploadBlobAndReturnUrl(fileName);
        }

        //=========================================================
        //          Delete blob 的檔案
        //=========================================================
        public ResultStuts DeleteBlob(string fileName, string container_name, string path)
        {
            //Console.WriteLine("Azure Blob storage - .NET Quickstart sample");
            //Console.WriteLine();
            //ProcessAsync().GetAwaiter().GetResult();

            ResultStuts resultStuts = new ResultStuts();
            resultStuts.CODE = "";
            resultStuts.MSG = "";

            //------------------------------------------------------
            BlobFunc blobFunc = new BlobFunc(Constants.STORAGE_CONNECTION_STRING, container_name, path);
            List<string> blobs = blobFunc.GetListBlobs();

            //------------------------------------------------------
            if (blobFunc.FindBlob(fileName) == 1)
            {
                blobFunc.DeleteBlob(fileName);
            }
            else
            {
                //Console.WriteLine("Can not find " + fileName + " file in BLOB_Server");
                resultStuts.CODE = "402";
                resultStuts.MSG = "Can not find " + fileName + " file in BLOB_Server\n";
            }



            if (resultStuts.CODE == "")
            {
                resultStuts.CODE = "200";
                resultStuts.MSG += "成功";
            }

            return resultStuts;
        }


        //=========================================================
        //          Delete blob 與 本機電腦中的檔案
        //=========================================================
        public ResultStuts DeleteContainerAndFolder(string fileName)
        {
            //Console.WriteLine("Azure Blob storage - .NET Quickstart sample");
            //Console.WriteLine();
            //ProcessAsync().GetAwaiter().GetResult();

            ResultStuts resultStuts = new ResultStuts();
            resultStuts.CODE = "";
            resultStuts.MSG = "";

            //------------------------------------------------------
            BlobFunc blobFunc = new BlobFunc(Constants.STORAGE_CONNECTION_STRING, Constants.CONTAINER_NAME_OATESTFILE, Constants.DISK_FOLDER_NAME_MAIN);
            List<string> blobs = blobFunc.GetListBlobs();


            //------------------------------------------------------
            FolderFunc folderFunc = new FolderFunc(Constants.DISK_FOLDER_NAME_MAIN);
            List<string> files = folderFunc.GetFolderEntries();

            //------------------------------------------------------
            if (blobFunc.FindBlob(fileName) == 1)
            {
                blobFunc.DeleteBlob(fileName);
            }
            else
            {
                //Console.WriteLine("Can not find " + fileName + " file in BLOB_Server");
                resultStuts.CODE = "402";
                resultStuts.MSG = "Can not find " + fileName + " file in BLOB_Server\n";
            }
            
            if (folderFunc.FindFile(fileName) == 1)
            {
                folderFunc.DeleteFile(fileName);
            }
            else
            {
                //Console.WriteLine("Can not find " + fileName + " file in disk");
                resultStuts.CODE = "402";
                resultStuts.MSG += "Can not find " + fileName + " file in disk";
            }

            if (resultStuts.CODE == "")
            {
                resultStuts.CODE = "200";
                resultStuts.MSG += "成功";
            }

            return resultStuts;
        }
    }
}
