﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easypay_BlobToDisk
{
    class Program
    {
        static void Main(string[] args)
        {   
            //office
            new FolderFunc(Constants.DISK_FOLDER_NAME_NEWCASE).CheckAndCreateFolder();
            new FolderFunc(Constants.DISK_FOLDER_NAME_OTHERCASE).CheckAndCreateFolder();
            new FolderFunc(Constants.DISK_FOLDER_NAME_PAYMENTCASE).CheckAndCreateFolder();

            //office backup
            new FolderFunc(Constants.DISK_FOLDER_NAME_NEWCASE_BACKUP).CheckAndCreateFolder();
            new FolderFunc(Constants.DISK_FOLDER_NAME_OTHERCASE_BACKUP).CheckAndCreateFolder();
            new FolderFunc(Constants.DISK_FOLDER_NAME_PAYMENTCASE_BACKUP).CheckAndCreateFolder();

            //test
            new FolderFunc(Constants.DISK_FOLDER_NAME_NEWCASE_TEST).CheckAndCreateFolder();
            new FolderFunc(Constants.DISK_FOLDER_NAME_OTHERCASE_TEST).CheckAndCreateFolder();
            new FolderFunc(Constants.DISK_FOLDER_NAME_PAYMENTCASE_TEST).CheckAndCreateFolder();
            
            //test backup
            new FolderFunc(Constants.DISK_FOLDER_NAME_NEWCASE_TEST_BACKUP).CheckAndCreateFolder();
            new FolderFunc(Constants.DISK_FOLDER_NAME_OTHERCASE_TEST_BACKUP).CheckAndCreateFolder();
            new FolderFunc(Constants.DISK_FOLDER_NAME_PAYMENTCASE_TEST_BACKUP).CheckAndCreateFolder();

            BlobMain blobMain = new BlobMain();

            blobMain.NotExistsCreate(Constants.CONTAINER_NAME_NEWCASE);
            blobMain.NotExistsCreate(Constants.CONTAINER_NAME_OTHERCASE);
            blobMain.NotExistsCreate(Constants.CONTAINER_NAME_PAYMENTCASE);

            //test
            blobMain.NotExistsCreate(Constants.CONTAINER_NAME_NEWCASE_TEST);
            blobMain.NotExistsCreate(Constants.CONTAINER_NAME_OTHERCASE_TEST);
            blobMain.NotExistsCreate(Constants.CONTAINER_NAME_PAYMENTCASE_TEST);

            //office
            blobMain.Download(Constants.CONTAINER_NAME_NEWCASE, Constants.DISK_FOLDER_NAME_NEWCASE, Constants.DISK_FOLDER_NAME_NEWCASE_BACKUP);
            blobMain.Download(Constants.CONTAINER_NAME_OTHERCASE, Constants.DISK_FOLDER_NAME_OTHERCASE, Constants.DISK_FOLDER_NAME_OTHERCASE_BACKUP);
            blobMain.Download(Constants.CONTAINER_NAME_PAYMENTCASE, Constants.DISK_FOLDER_NAME_PAYMENTCASE, Constants.DISK_FOLDER_NAME_PAYMENTCASE_BACKUP);

            //test
            blobMain.Download(Constants.CONTAINER_NAME_NEWCASE_TEST, Constants.DISK_FOLDER_NAME_NEWCASE_TEST, Constants.DISK_FOLDER_NAME_NEWCASE_TEST_BACKUP);
            blobMain.Download(Constants.CONTAINER_NAME_OTHERCASE_TEST, Constants.DISK_FOLDER_NAME_OTHERCASE_TEST, Constants.DISK_FOLDER_NAME_OTHERCASE_TEST_BACKUP);
            blobMain.Download(Constants.CONTAINER_NAME_PAYMENTCASE_TEST, Constants.DISK_FOLDER_NAME_PAYMENTCASE_TEST, Constants.DISK_FOLDER_NAME_PAYMENTCASE_TEST_BACKUP);

           
            //等全部ZIP檔案下載成功後 下載圖片
            //blobMain.Upload();
            //blobMain.Delete();
        }
    }
}
