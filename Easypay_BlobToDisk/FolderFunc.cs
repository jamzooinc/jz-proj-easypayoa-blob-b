﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Easypay_BlobToDisk
{
    public class FolderFunc
    {
        //==============================================
        //         Register
        //==============================================
        private string PATH = "";//"F:\\asp_download";



        //==============================================
        //         initial
        //==============================================
        public FolderFunc(string path)
        {
            this.PATH = path;
        }

        //==============================================
        //         initial
        //==============================================
        public void CheckAndCreateFolder()
        {
            bool exists = System.IO.Directory.Exists(PATH);

            if (!exists)
                System.IO.Directory.CreateDirectory(PATH);
        }


        //==============================================
        //         
        //==============================================
        public List<string> GetFolderEntries()
        {
            List<string> files = new List<string>();
            string folder = PATH + "\\";


            foreach (string fname in System.IO.Directory.GetFileSystemEntries(PATH))
            {
                if (fname.IndexOf(folder) == 0)
                {
                    string file_name = fname.Remove(0, folder.Length);
                    files.Add(file_name);
                }

            }
            return files;
        }

        //==============================================
        //         
        //==============================================
        public int FindFile(string fileName)
        {
            List<string> files = GetFolderEntries();

            int yes = 0;
            foreach (string file in files)
            {
                if (file == fileName) yes = 1;
            }
            return yes;
        }

        //==============================================
        //         
        //==============================================
        public void DeleteFile(string fileName)
        {
            File.Delete(PATH + "\\" + fileName);
        }



    }
}
